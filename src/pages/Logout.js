import {useContext, useEffect} from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){

	//consume the UserContext object and destructure it to access the user state and unsetUSer function from the context provider

	const { unsetUser, setUser} = useContext(UserContext);

	/*localStorage.clear();*/
	//clear the localStorage of the user's info
	unsetUser();

	useEffect(()=>{

		setUser({id:null})

	}, [])

	return(

		<Navigate to="/"/>

	)
}
