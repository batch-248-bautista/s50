import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


// import CourseCard from '../components/CourseCard';


/*export default function Home(){
	
	return (
		<>
			<Banner
				title = "B248 Booking App"
				subtitle = "Opportunities for everyone, everywhere!"
				buttonText = "Enroll Now!"
				buttonVariant ="primary"
				/>
    		<Highlights/>
		</>
	)
}*/
const data = {
	title: "Zuitt Coding Bootcamp",
	content: "Opportunities for everyone, everywhere",
	destination: "/courses",
	label: "Enroll now!"
}


export default function Home(){

	return (
		<>
			<Banner data={data}/>
    		<Highlights/>
    		{/*<CourseCard/>*/}
		</>
	)
}

