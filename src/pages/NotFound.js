/*import { useState } from "react";*/
/*import {Navigate} from 'react-router-dom';*/
/*import {Button} from 'react-bootstrap';*/
import Banner from '../components/Banner';

/*export default function NotFound(){
	const [redirect, setRedirect] = useState(false);
i
	if (redrect){
		return <Navigate to ="/" />;
	}
	const buttonClick = () => {
		setRedirect(true);
	}

	return(
		<>
		<Banner
		title="Page Not Found"
		subtitle="The page you are looking for cannot be found."
		buttonText="Back to Home"
		buttonVariant="primary"
		onClick = {buttonClick}
		/>
		</>
		)
}*/

export default function Error(){

	const data = {

		title: "404 - Page Not Found",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back to Home"

	}

	return (

		<Banner data={data}/>

	)

}
