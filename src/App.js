import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';

import { BrowserRouter as Router } from 'react-router-dom';
/*
  BrowserRouter is a component provided by the React Router library
  Allows us to build SPA with multiple views or pages that can be navigated without refreshing the browser
*/
import { Routes, Route } from 'react-router-dom';
import CourseView from './components/CourseView';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import './App.css';

//import UserProvider from userCOntext
import {UserProvider} from './UserContext';

function App() {
  //define astate hook
  //this will be used to store information and will be used for validating if a user is logged in on the app or not

  // state hook for theuser state that's defined here forthe global scope
  // we created a state that will beused all throughout our components in out application

  const [user, setUser] = useState({
    id: null,
    isAdmin:null
  });

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(()=>{
    fetch(`http://localhost:4000/users/details`,{
      headers:{
        Authorization:`Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=>res.json())
    .then(data => {
      console.log(data);

      if(typeof data._id !== "undefined"){
        setUser ({
          id : data._id,
          isAdmin : data.isAdmin
        })
      }
      else{
        setUser ({
          id : null,
          isAdmin : null
        })
      }
    })
  },[])
  //wrap all the components wihtin the userProvider to allow re-rendering when the value cahnges
  return (
    <UserProvider value={{ user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container>
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/courses" element={<Courses/>} />
            <Route path="/courses/:courseId" element={<CourseView/>}/>
            <Route path="/login" element={<Login/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="*" element={<NotFound/>} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
