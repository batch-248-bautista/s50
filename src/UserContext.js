//Use React's COntext API to give the authenticated user  object to have "global" scope within our application
import React from 'react';

//creation of contexr object
//a context ibject is a datatype of an object that canbe used to store information that can be shared to other components within the app
//context object is a difeerent approacj to passing info between components and allows easier access by avoiding rhe use of prop-drilling


const UserContext = React.createContext();
/*
	a cotext object is created using the React.createContext() method, which returns an object with 2 properties:
	1. Provider - is used to wrap the components that need access to the context, and it provides the contect value to all of its descentants
	2. Consumer - is used to access the context value within a component
	
*/

//the provider component allows other components to consume n/use the context object and supply the necessary information needed.
export const UserProvider = UserContext.Provider;

export default UserContext;